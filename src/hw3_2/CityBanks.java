package hw3_2;


import java.util.Scanner;

public class CityBanks {
    private Bank[] banks;

    Scanner scan = new Scanner(System.in);
    int amount = scan.nextInt();
    String currency = scan.next();
    String course = scan.next();
    String how = scan.next();

    public CityBanks() {
        banks = generate();
    }

    private Bank[] generate() {
        Bank[] banks = new Bank[3];

        banks[0] = new Bank("PrivatBank", new float[]{24.5f, 27f}, new float[]{27.4f, 30.3f}, new float[]{0.44f, 0.53f});
        banks[1] = new Bank("OshadBank", new float[]{23.3f, 26.9f}, new float[]{28.2f, 31f}, new float[]{0.45f, 0.52f});
        banks[2] = new Bank("PUMB", new float[]{24f, 27.8f}, new float[]{28.9f, 32.2f}, new float[]{0.43f, 0.48f});

        return banks;
    }


    public void findConverted() {
        for (Bank bank : banks) {
            if (currency.equals(bank.name)) {
                //System.out.println(amount / bank.courseUSD);
                switch (course) {
                    case "USD":
                        if (how.equals("pokupka")) {
                            System.out.println(amount * bank.courseUSD[0]);
                        } else if (how.equals("prodaga")) {
                            System.out.println(amount / bank.courseUSD[1]);
                        }
                        break;
                    case "EUR":
                        if (how.equals("pokupka")) {
                            System.out.println(amount * bank.courseEUR[0]);
                        } else if (how.equals("prodaga")) {
                            System.out.println(amount / bank.courseEUR[1]);
                        }
                        break;
                    case "RUB":
                        if (how.equals("pokupka")) {
                            System.out.println(amount * bank.courseRUB[0]);
                        } else if (how.equals("prodaga")) {
                            System.out.println(amount / bank.courseRUB[1]);
                        }
                        break;
                }
            }

        }


    }
}
